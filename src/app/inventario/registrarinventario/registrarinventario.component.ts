import { Component, OnInit } from '@angular/core';
import { Docente } from '../../docente/docente';
import { DocenteService } from '../docente.service';
import { Router } from '@angular/router';
import { Articulo } from '../../articulo/articulo';
import { Inventario } from '../inventario';
import { ArticuloService } from '../articulo.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { InventarioService } from '../inventario.service';
import { QueryOptions } from 'src/app/spring-generic-mvc/model';

@Component({
  selector: 'app-registrarinventario',
  templateUrl: './registrarinventario.component.html',
  styleUrls: ['./registrarinventario.component.css']
})
export class RegistrarinventarioComponent implements OnInit {

  //Listar docentes
  docentes = new Array<Docente>();
  parametro: string;
  dia: number;
  mes: string;
  mm: number;
  aa: number;
  fecha: string;
  hoy = new Date();
  bandera1 = true;
  bandera2 = false;
  bandera3 = false;
  bandter = false;
  activo = false;
  mensaje: string;
  ceduladoc: string;
  doc: Docente;
  art;

  cols: any[];
  cols2: any[];
  loading = true;
  totalrecords;
  totaldocentes;
  numerofilas = 10;
  pagina = 0;
  selectedArticulo: Articulo;
  //Listar articulo
  articulos = new Array<Articulo>();
  // Ingresar inventario
  addForm: FormGroup;
  submitted = false;
  //Listar inventario
  inventarios = new Array<Inventario>();

  constructor(private invService: InventarioService, private docService: DocenteService, private artService: ArticuloService, private router: Router, private formBuilder: FormBuilder) { }



  ngOnInit() {
    this.dia = this.hoy.getDate();
    this.mm = this.hoy.getMonth() + 1;
    this.aa = this.hoy.getFullYear();
    this.fecha = this.dia + "/" + this.mm + "/" + this.aa;
    if (this.mm == 1) {
      this.mes = "enero";
    }
    if (this.mm == 2) {
      this.mes = "febrero";
    }
    if (this.mm == 3) {
      this.mes = "marzo";
    }
    if (this.mm == 4) {
      this.mes = "abril";
    }
    if (this.mm == 5) {
      this.mes = "mayo";
    }
    if (this.mm == 6) {
      this.mes = "junio";
    }
    if (this.mm == 7) {
      this.mes = "julio";
    }
    if (this.mm == 8) {
      this.mes = "agosto";
    }
    if (this.mm == 9) {
      this.mes = "septiembre";
    }
    if (this.mm == 10) {
      this.mes = "octubre";
    }
    if (this.mm == 11) {
      this.mes = "noviembre";
    }
    if (this.mm == 12) {
      this.mes = "diciembre";
    }
    this.listarDocente();
  }

  loadArticulosLazy(event) {
    this.loading = true;

    setTimeout(() => {
      if (this.articulos) {
        const q: QueryOptions = new QueryOptions();
        //El archivo para paginar los articulos realizado en spring tools
        q.pageSize = event.rows;
        // Calculo de pagina
        let page = Math.ceil(event.first / this.numerofilas);
        this.pagina = page;
        if (page < 0) {
          page = 0;
        }
        q.pageNumber = page;
        // Sorts
        q.sortField = event.sortField;
        q.sortOrder = event.sortOrder;
        console.log(event);
        if (event.filters.global) {
          this.artService.find(q, event.filters.global.value).subscribe(resp => {
            // console.log(resp);
            this.articulos = resp.content;
            this.totalrecords = resp.totalElements * 1;
          });
        } else {
          this.artService.list(q).subscribe(resp => {
            console.log(resp);
            this.articulos = resp.content;
            this.totalrecords = resp.totalElements * 1;
            console.log(this.totalrecords)
          });
        }

        this.loading = false;
      }
    }, 1000);
  }

  loadDocentesLazy(event) {
    this.loading = true;

    setTimeout(() => {
      if (this.docentes) {
        const q: QueryOptions = new QueryOptions();
        //El archivo para paginar los articulos realizado en spring tools
        q.pageSize = event.rows;
        // Calculo de pagina
        let page = Math.ceil(event.first / this.numerofilas);
        this.pagina = page;
        if (page < 0) {
          page = 0;
        }
        q.pageNumber = page;
        // Sorts
        q.sortField = event.sortField;
        q.sortOrder = event.sortOrder;
        console.log(event);
        if (event.filters.global) {
          this.docService.find(q, event.filters.global.value).subscribe(resp => {
            // console.log(resp);
            this.docentes = resp.content;
            this.totaldocentes = resp.totalElements * 1;
          });
        } else {
          this.docService.list(q).subscribe(resp => {
            console.log(resp);
            this.docentes = resp.content;
            this.totaldocentes = resp.totalElements * 1;
            console.log(this.totalrecords)
          });
        }

        this.loading = false;
      }
    }, 1000);
  }

  columnFilter(event: any) {
    console.log(event);
    console.log();
  }

  onRowSelect(e) {
    // console.log(this.ProductoSeleccionado);
  }

  listarArticulo() { // Funciona
    const q: QueryOptions = new QueryOptions();
    q.pageSize = this.numerofilas;
    q.pageNumber = this.pagina;
    this.artService.list(q).subscribe(resp => {
      this.articulos = resp.content;
      this.totalrecords = resp.totalElements;
    });
  }

  buscarporCodigo() { // Funciona
    this.artService.buscarporCodigo(this.parametro).subscribe(data => {
      this.articulos = data;
    });
  }
  buscarporAula() { // Funciona
    this.artService.buscarporAula(this.parametro).subscribe(data => {
      this.articulos = data;
    });
  }

  listarDocente() { // Funciona -ok
    const q: QueryOptions = new QueryOptions();
    q.pageSize = this.numerofilas;
    q.pageNumber = this.pagina;
    this.docService.list(q).subscribe(resp => {
      this.docentes = resp.content;
      this.totaldocentes = resp.totalElements;
    });
  }

  buscarporParametro() { // Funciona -ok
    this.docService.buscarporParametro(this.parametro).subscribe(data => {
      this.docentes = data;
    });
  }

  seleccionarDocente(doc) {
    this.bandera1 = false;
    this.bandera2 = true;
    this.bandter = false;
    this.activo = false;
    console.log(doc);
    this.doc = doc;
    this.ceduladoc = doc.per_identificacion;
    this.bandera1 = false;
    this.bandera2 = true;
    this.bandter = false;
    this.activo = false;
    this.listarArticulo();
  }

  seleccionarArticulo(art) {
    var fechaAct = new Date().toLocaleDateString();
    this.art = art;

    this.artService.editarporCodigo("ASIGNADO", art.art_codigo)
      .subscribe(data => {
        //Ingresa inventario

        
        this.addForm = this.formBuilder.group({
          inv_id: [],
          inv_doc_cedula: [this.ceduladoc],
          inv_art_codigo: [this.art.art_codigo],
          inv_art_bien: [this.art.art_bien],
          inv_art_color: [this.art.art_color],
          inv_art_marca: [this.art.art_marca],
          inv_art_modelo: [this.art.art_modelo],
          inv_art_serie: [this.art.art_serie],
          inv_estado: [this.art.art_estado],
          inv_art_ubicacion_destino: [this.art.art_aula],
          inv_fecha: [this.fecha]
        });
        console.log(this.art);
        this.invService.crearInventario(this.addForm.value).subscribe(data => {
          this.bandter = true;
          this.bandera1 = false;
          this.bandera2 = true;
          this.activo = false;
          this.listarArticulo();
        });
      });
  }

  terminarRegistro() {
    this.bandera1 = false;
    this.bandera2 = false;
    this.bandera3 = true;
    this.activo = true;
    this.bandter = false;
    console.log(this.doc.per_identificacion);
    this.invService.buscarporCedula(this.doc.per_identificacion).subscribe(data => {
      this.inventarios = data;
    });

  }

  rootear() {
    this.router.navigate(['/inventario/listar']);
  }

}