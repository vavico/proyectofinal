import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListardocenteComponent } from './docente/listardocente/listardocente.component';
import { ListarinventarioComponent } from './inventario/listarinventario/listarinventario.component';
import { RegistrarinventarioComponent } from './inventario/registrarinventario/registrarinventario.component';
import { ListararticuloComponent } from './articulo/listararticulo/listararticulo.component';
import { RegistrararticuloComponent } from './articulo/registrararticulo/registrararticulo.component';
import { ActualizararticuloComponent } from './articulo/actualizararticulo/actualizararticulo.component';
import { LoginComponent } from './login/login.component';
import { RegistracategoriaComponent } from './categoria/registracategoria/registracategoria.component';
import { ListarcategoriaComponent } from './categoria/listarcategoria/listarcategoria.component';
import { DepreciacionComponent } from './articulo/depreciacion/depreciacion.component';
import { ListarbodegaComponent } from './bodega/listarbodega/listarbodega.component';
import { RegistrarbodegaComponent } from './bodega/registrarbodega/registrarbodega.component';
import {ModuleWithProviders} from '@angular/core';
import { ModuloComponent } from './Inicio/modulo/modulo.component';
import { VistaComponent } from './soporte/vista/vista.component';
import { ResgistrarinstitucionComponent } from './institucion/resgistrarinstitucion/resgistrarinstitucion.component';
import { ListarinstitucionComponent } from './institucion/listarinstitucion/listarinstitucion.component';

const routes: Routes = [

 {path: 'Inicio', 
  component: ModuloComponent,  
  pathMatch: 'full'
  },
  { path: 'login', 
  component: LoginComponent,  
  pathMatch: 'full'
  },
  {
    path: 'docente/listar',
    component: ListardocenteComponent
  },
  {
    path: 'inventario/guardar',
    component: RegistrarinventarioComponent
  },
  {
    path: 'inventario/listar',
    component: ListarinventarioComponent
  },
  {
    path: 'categoria/listar',
    component: ListarcategoriaComponent
  },
  {
    path: 'categoria/guardar',
    component: RegistracategoriaComponent
  },
  {
    path: 'bodega/listar',
    component: ListarbodegaComponent
  },
  {
    path: 'bodega/guardar',
    component: RegistrarbodegaComponent
  },
  // Crear
  {
    path: 'articulo/guardar',
    component: RegistrararticuloComponent
  },
  {
    path: 'articulo/depreciacion',
    component: DepreciacionComponent
  } ,
  {
    path: 'articulo/editar',
    component: ActualizararticuloComponent
  },
  // Listar
  {
    path: 'articulo/listar',
    component: ListararticuloComponent
  },
  {
    path: 'Soporte',
    component: VistaComponent
  },
  {
    path: 'institucion/ingresar',
    component: ResgistrarinstitucionComponent
  },
  {
    path: 'institucion/listar',
    component: ListarinstitucionComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'});