export interface Docente{
    per_identificacion: string;
    per_primerapellido: String;
    per_primernombre: String;
    per_segundonombre: String;
    per_celular: String;
    per_correo: String;
    per_fechanacimiento: string;
}