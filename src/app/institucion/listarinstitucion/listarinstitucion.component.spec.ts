import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarinstitucionComponent } from './listarinstitucion.component';

describe('ListarinstitucionComponent', () => {
  let component: ListarinstitucionComponent;
  let fixture: ComponentFixture<ListarinstitucionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarinstitucionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarinstitucionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
