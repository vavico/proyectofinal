import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResgistrarinstitucionComponent } from './resgistrarinstitucion.component';

describe('ResgistrarinstitucionComponent', () => {
  let component: ResgistrarinstitucionComponent;
  let fixture: ComponentFixture<ResgistrarinstitucionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResgistrarinstitucionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResgistrarinstitucionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
