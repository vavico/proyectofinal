import { almacen } from '../almacen/almacen';

export interface bodega{
    aula_id : number;
    aula_nombre : String;
    aula_edif_id : almacen;
}