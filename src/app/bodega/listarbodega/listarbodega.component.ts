import { Component, OnInit } from '@angular/core';
import { bodega } from '../bodega';
import { BodegasService } from '../bodegas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listarbodega',
  templateUrl: './listarbodega.component.html',
  styleUrls: ['./listarbodega.component.css']
})
export class ListarbodegaComponent implements OnInit {

  //Listar categoria
  bodegas = new Array<bodega>();
  nombre: string;
  public catEditar: bodega;
  bandera= false;
  cols: any[];
  selectedCar;
  cars;
  cars1;
  articuloEditar;
  constructor(private bodService: BodegasService, private router: Router) {  }

  ngOnInit() {
    this.listarBodega();
    
    this.cols =[
      { field: 'aula_id', header: 'ID'},
      { field: 'aula_nombre', header: 'Nombre'},
      { field: 'aula_edif_id', header: 'Almacen'},

    ];
  }

  listarBodega(){ // Funciona
    this.bodService.listarCategoria().subscribe(data =>{
      this.bodegas = data;
    });
  }
  
  buscarporNombre(){ // Funciona
    this.bodService.buscarporNombre(this.nombre).subscribe(data =>{
    this.bodegas = data;
    });
  }

  editarporId(bod: bodega){ 
     this.bandera=true;
     this.catEditar= bod;
     console.log(this.catEditar, "desde listar");
  }

  eliminarporId(bod: bodega){ // Funciona
     this.bodService.eliminarporId(bod.aula_id).subscribe(data =>{
       alert("Eliminado");
       this.listarBodega();
      });
  }

  volver(){
    this.bandera=false;
    this.listarBodega();
  }

}
