export interface Articulo{
    art_id : number;
    art_codigo : String;
    art_cod_anterior : String;
    art_numero_acta : String;
    art_bld : String;
    art_bien : String;
    art_serie : String;
    art_modelo : String;
    art_marca : String;
    art_color : String;
    art_material : String;
    art_descripcion:String;
    art_dimensiones : String;
    art_aula : String;
    art_cuenta_contable : String;
    art_fecha_ing : String;
    art_fecha_depre : String;
    art_valor_contable : String;
    art_observacion : String;
    art_conciliacion : String;
    art_estado : String;
    art_categoria : String;
    art_empresa : String;
    art_asignado : String;
}

