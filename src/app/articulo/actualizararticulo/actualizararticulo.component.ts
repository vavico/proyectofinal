import { Component,Input, OnInit } from '@angular/core';
import { Articulo } from '../articulo';
import { ArticuloService } from '../articulo.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-actualizararticulo',
  templateUrl: './actualizararticulo.component.html',
  styleUrls: ['./actualizararticulo.component.css']
})
export class ActualizararticuloComponent implements OnInit {

  @Input() art: Articulo;
  addForm: FormGroup;
  submitted = false;
  asignado = false;
  bandera= false;
  mensaje: string;
  constructor(private artservice: ArticuloService, private router: Router, private formBuilder: FormBuilder) { 
  }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      art_id: [],
      art_codigo:[this.art.art_codigo,Validators.required],
      art_estado: [this.art.art_estado, [Validators.required, Validators.required]],
      art_asignado: [this.art.art_asignado]
    });
  }

  get f(){return this.addForm.controls;}

  onSubmit(){
    this.submitted = true;
    if (this.addForm.invalid){
      return;
    }
    this.artservice.editarporId(this.addForm.value, this.art.art_id)
    .subscribe(data => {
      console.log("editado :D");
      this.mensaje="Datos actualizados";
      this.bandera=true;
    });
  }
}