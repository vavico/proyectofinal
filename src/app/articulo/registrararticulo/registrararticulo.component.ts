import { Component, OnInit } from '@angular/core';
import { ArticuloService } from '../articulo.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registrararticulo',
  templateUrl: './registrararticulo.component.html',
  styleUrls: ['./registrararticulo.component.css']
})
export class RegistrararticuloComponent implements OnInit {

  date1: Date;
  date2: Date;
  
  constructor(private artservice: ArticuloService, private router: Router, private formBuilder: FormBuilder) { }
  addForm: FormGroup;
  submitted = false;
  ngOnInit() {
    this.addForm = this.formBuilder.group({
      art_id: [],
      art_codigo: ['', Validators.required],
      art_cod_aterior: ['', Validators.required],
      art_num_acta: ['', Validators.required],
      art_bld: ['', Validators.required],
      art_bien: ['', Validators.required],
      art_serie: ['', Validators.required],
      art_modelo: ['', Validators.required],
      art_marca: ['', Validators.required],
      art_color: ['', Validators.required],
      art_material: ['', Validators.required],
      art_dimensiones: ['', Validators.required],
      art_edificio: ['', Validators.required],
      art_aula: ['', Validators.required],
      art_cuenta_contable: ['', Validators.required],
      art_fecha_ing: ['', Validators.required],
      art_fecha_depre: ['', Validators.required],
      art_valor_contable: ['', Validators.required],
      art_observacion: ['', Validators.required],
      art_conciliacion: ['', Validators.required],
      art_estado: ['', Validators.required],
      art_categoria: ['', Validators.required],
      art_precio: ['', Validators.required],
      art_empresa: ['', Validators.required],
      art_asignado: ['NO ASIGNADO']
    });

  }
  get f() { return this.addForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.artservice.crearArticulo(this.addForm.value)
      .subscribe(data => {
        alert("Artículo guardado con éxito");
        this.router.navigate(['/articulo/listar']);
      });

  }

}

