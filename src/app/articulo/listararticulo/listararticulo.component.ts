import { Component, OnInit } from '@angular/core';
import { Articulo } from '../articulo';
import { ArticuloService } from '../articulo.service';
import { Router } from '@angular/router';
import { QueryOptions } from 'src/app/spring-generic-mvc/model';

@Component({
  selector: 'app-listararticulo',
  templateUrl: './listararticulo.component.html',
  styleUrls: ['./listararticulo.component.css'],
})

export class ListararticuloComponent implements OnInit {

  //Listar articulo
  articulos = new Array<Articulo>();
  codigo: string;
  articuloEditar: Articulo;
  selectedArtic: any[];

  bandera = false;
  cols: any[];

  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;

  constructor(private artService: ArticuloService, private router: Router) { }

  ngOnInit() {
    //this.listarArticulo();
    
    this.cols = [
      { field: 'art_codigo', header: 'Código'},
      { field: 'art_cod_anterior', header: 'Código Anterior' },
      { field: 'art_numero_acta', header: 'Num. Acta' },
      { field: 'art_bld', header: 'BLD/BCA' },
      { field: 'art_bien', header: 'Bien' },
      { field: 'art_serie', header: 'Serie' },
      { field: 'art_modelo', header: 'Modelo' },
      { field: 'art_marca', header: 'Marca' },
      { field: 'art_color', header: 'Color' },
      { field: 'art_material', header: 'Material' },
      { field: 'art_dimensiones', header: 'Dimensiones' },
      { field: 'art_aula', header: 'Aula' },
      //{ field: 'art_descripcion', header: 'Descripcion' },
      { field: 'art_cuenta_contable', header: 'Cuenta Contable' },
      { field: 'art_fecha_ing', header: 'Fecha Ing.' },
      { field: 'art_fecha_depre', header: 'Fecha Ult. Depreciación' },
      { field: 'art_valor_contable', header: 'Valor Contable' },
      { field: 'art_observacion', header: 'Observación' },
      { field: 'art_conciliación', header: 'Conciliación' },
      { field: 'art_estado', header: 'Estado' },
      { field: 'art_categoria', header: 'Categoria' },
      { field: 'art_empresa', header: 'Empresa' },
      { field: 'art_asignado', header: 'Asignado' },
    ];
    // método para cargar los módulos paginados
    // metodos para listar lar relaciones
    //  const q: QueryOptions = new QueryOptions(); // documento jqey donde se encuentra paginado con el load lazy
    // q.pageSize = this.numerofilas;
    // q.pageNumber = this.pagina;
    // this.artService.list(q).subscribe(resp => {
    // this.articulos = resp.content;
    this.listar();
    //this.mySelect.ngOnInit()
    //  }
    //  );
    this.selectedArtic=this.cols;

  }

  //eventos de load lazy

  loadCarsLazy(event) {
    this.loading = true;

    // in a real application, make a remote request to load data using state metadata from event
    // event.first = First row offset
    // event.rows = Number of rows per page
    // event.sortField = Field name to sort with
    // event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    // filters: FilterMetadata object having field as key and filter value, filter matchMode as value

    setTimeout(() => {
      if (this.articulos) {
        const q: QueryOptions = new QueryOptions(); //El archivo para paginar los articulos realizado en spring tools
        q.pageSize = event.rows;
        // Calculo de pagina
        let page = Math.ceil(event.first / this.numerofilas);
        this.pagina = page;
        if (page < 0) {
          page = 0;
        }
        q.pageNumber = page;
        // Sorts
        q.sortField = event.sortField;
        q.sortOrder = event.sortOrder;
        console.log(event);
        if (event.filters.global) {
          this.artService.find(q, event.filters.global.value).subscribe(resp => {
            // console.log(resp);
            this.articulos = resp.content;
            this.totalrecords = resp.totalElements*1;
          });
        } else {
          this.artService.list(q).subscribe(resp => {
             console.log(resp);
            this.articulos = resp.content;
            this.totalrecords = resp.totalElements*1;
            console.log(this.totalrecords)
          });
        }

        this.loading = false;
      }
    }, 1000);
  }

  columnFilter(event: any) {
    console.log(event);
    console.log();
  }

  onRowSelect(e) {
    // console.log(this.ProductoSeleccionado);
  }

  listar() {
    const q: QueryOptions = new QueryOptions();
    q.pageSize = this.numerofilas;
    q.pageNumber = this.pagina;
    this.artService.list(q).subscribe(resp => {
      this.articulos = resp.content;
      this.totalrecords = resp.totalElements;
    });
  }


  // listarArticulo() { // Funciona
  //   this.artService.listarArticulo().subscribe(data => {
  //     this.articulos = data;
  //   });
  // }

  buscarporCodigo() { // Funciona
    this.artService.buscarporCodigo(this.codigo).subscribe(data => {
      this.articulos = data;
    });
  }

  editarporId(art: Articulo) {
    if (art.art_asignado == "ASIGNADO") {
      alert("El articulo se encuentra asignado, no se permite editar");
    } else {
      this.bandera = true;
      this.articuloEditar = art;
      console.log(this.articuloEditar, "desde listar");
    }
  }

  eliminarporId(art: Articulo) { // Funciona
    if (art.art_asignado == "ASIGNADO") {
      alert("El articulo se encuentra asignado, no se permite eliminar");
    } else {
      this.artService.eliminarporId(art.art_id).subscribe(data => {
        alert("Eliminado");
        this.listar();
      });
    }

  }

  volver() {
    this.bandera = false;
    this.listar();
  }

}